/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.os.Bundle;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Martin Albedinsky
 */
public class AccountResultsTest extends AndroidTestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		AccountResults.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<AccountResults> constructor = AccountResults.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testEmpty() {
		// Act:
		final AccountResult result = AccountResults.empty();
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getValue(), is(Bundle.EMPTY));
		assertThat(result.isFailure(), is(false));
		assertThat(result.getError(), is(AccountError.NONE));
		assertThat(result.toString(), is("AccountResult(isSuccess: true, value: " + Bundle.EMPTY.toString() + ", isFailure: false, error: NONE)"));
		assertThat(result, is(AccountResults.empty()));
	}
}