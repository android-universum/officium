/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

import androidx.annotation.NonNull;
import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.Cryptography;
import universum.studios.android.crypto.CryptographyException;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.crypto.util.CryptographyUtils;

/**
 * @author Martin Albedinsky
 */
final class TestCryptography {

    private TestCryptography() {}

    @NonNull static byte[] decode(@NonNull final byte[] data) {
        return Base64.decode(data, Base64.DEFAULT);
    }

    @NonNull static byte[] decode(@NonNull final String value) throws UnsupportedEncodingException {
        return Base64.decode(value.getBytes(Cryptography.CHARSET_NAME), Base64.DEFAULT);
    }

    @NonNull static String encryptToString(@NonNull final String value) {
        return CryptographyUtils.encrypt(value, createCrypto());
    }

    @NonNull static String decryptToString(@NonNull final String value) {
        return CryptographyUtils.decrypt(value, createCrypto());
    }

    @NonNull static byte[] encrypt(@NonNull final String value) throws UnsupportedEncodingException {
        return createCrypto().encrypt(bytesOf(value));
    }

    @NonNull static byte[] decrypt(@NonNull final String value) throws UnsupportedEncodingException {
        return createCrypto().decrypt(bytesOf(value));
    }

    @NonNull static byte[] bytesOf(@NonNull final String value) throws UnsupportedEncodingException {
        return value.getBytes(Cryptography.CHARSET_NAME);
    }

    @NonNull static Crypto createCrypto() {
        return new Crypto() {

            @Override @NonNull public byte[] encrypt(@NonNull final byte[] data) throws CryptographyException {
                return Base64.encode(data, Base64.DEFAULT);
            }

            @Override @NonNull public byte[] decrypt(@NonNull final byte[] data) throws CryptographyException {
                return Base64.decode(data, Base64.DEFAULT);
            }
        };
    }

    @NonNull static Encrypto createEncrypto() {
        return new Encrypto() {

            @Override @NonNull public byte[] encrypt(@NonNull final byte[] data) throws CryptographyException {
                return Base64.encode(data, Base64.DEFAULT);
            }
        };
    }
}