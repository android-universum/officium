/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import org.junit.Test;
import org.mockito.Mockito;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.crypto.util.CryptographyUtils;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Martin Albedinsky
 */
public class BaseAccountManagerTest extends AndroidTestCase {

	@Test public void testContract() {
		// Assert:
		// - permissions:
		assertThat(BaseAccountManager.PERMISSION_GET_ACCOUNTS, is(Manifest.permission.GET_ACCOUNTS));
		assertThat(BaseAccountManager.PERMISSION_MANAGE_ACCOUNTS, is("android.permission.MANAGE_ACCOUNTS"));
		assertThat(BaseAccountManager.PERMISSION_AUTHENTICATE_ACCOUNTS, is("android.permission.AUTHENTICATE_ACCOUNTS"));
		// - errors:
		assertThat(BaseAccountManager.ERROR_ACCOUNT_DOES_NOT_EXIST, is("universum.studios.android.officium.account.AccountManager.ERROR.AccountDoesNotExist"));
		assertThat(BaseAccountManager.ERROR_SYSTEM_OPERATION_FAILURE, is("universum.studios.android.officium.account.AccountManager.ERROR.SystemOperationFailure"));
	}

	@Test public void testInstantiation() {
		// Act:
		final BaseAccountManager manager = new TestManager(context());
		// Assert:
		assertThat(manager.getAccountType(), is(TestManager.ACCOUNT_TYPE));
		assertThat(manager.getSystemManager(), is(AccountManager.get(context())));
	}

	@Test public void testObserverRegistration() {
		// Arrange:
		final TestAccount account = new TestAccount("Test");
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final TestObserver mockObserverFirst = mock(TestObserver.class);
		final TestObserver mockObserverSecond = mock(TestObserver.class);
		// Act + Assert:
		manager.registerObserver(mockObserverFirst);
		manager.registerObserver(mockObserverFirst);
		manager.registerObserver(mockObserverSecond);
		manager.notifyAccountAdded(account);
		manager.notifyAccountUpdated(account);
		manager.notifyAccountRemoved(account);
		verify(mockObserverFirst).onAccountAdded(account);
		verify(mockObserverSecond).onAccountAdded(account);
		verify(mockObserverFirst).onAccountUpdated(account);
		verify(mockObserverSecond).onAccountUpdated(account);
		verify(mockObserverFirst).onAccountRemoved(account);
		verify(mockObserverSecond).onAccountRemoved(account);
		verifyNoMoreInteractions(mockObserverFirst, mockObserverSecond);
		Mockito.clearInvocations(mockObserverFirst, mockObserverSecond);
		manager.unregisterObserver(mockObserverFirst);
		manager.notifyAccountAdded(account);
		manager.notifyAccountUpdated(account);
		manager.notifyAccountRemoved(account);
		verify(mockObserverSecond).onAccountAdded(account);
		verify(mockObserverSecond).onAccountUpdated(account);
		verify(mockObserverSecond).onAccountRemoved(account);
		verifyNoMoreInteractions(mockObserverFirst, mockObserverSecond);
		Mockito.clearInvocations(mockObserverFirst, mockObserverSecond);
		manager.unregisterObserver(mockObserverSecond);
		manager.notifyAccountAdded(account);
		manager.notifyAccountUpdated(account);
		manager.notifyAccountRemoved(account);
		verifyNoMoreInteractions(mockObserverFirst, mockObserverSecond);
	}

	@Test public void testEncryptBundle() throws UnsupportedEncodingException {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final String keyFirst = "KEY.First";
		final String keySecond = "KEY.Second";
		final String keyThird = "KEY.Third";
		final byte[] keyFirstEncrypted = TestCryptography.encrypt(keyFirst);
		final byte[] keySecondEncrypted = TestCryptography.encrypt(keySecond);
		final byte[] keyThirdEncrypted = TestCryptography.encrypt(keyThird);
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyFirst))).thenReturn(keyFirstEncrypted);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keySecond))).thenReturn(keySecondEncrypted);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyThird))).thenReturn(keyThirdEncrypted);
		manager.setKeyEncrypto(mockKeyEncrypto);
		final String dataFirst = "DATA.First";
		final String dataSecond = "DATA.Second";
		final byte[] dataFirstEncrypted = TestCryptography.encrypt(dataFirst);
		final byte[] dataSecondEncrypted = TestCryptography.encrypt(dataSecond);
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataFirst))).thenReturn(dataFirstEncrypted);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataSecond))).thenReturn(dataSecondEncrypted);
		manager.setCrypto(mockCrypto);
		final Bundle bundle = new Bundle();
		bundle.putString(keyFirst, dataFirst);
		bundle.putString(keySecond, dataSecond);
		bundle.putString(keyThird, null);
		// Act:
		final Bundle encryptedBundle = manager.encryptBundle(bundle);
		// Assert:
		assertThat(encryptedBundle, is(notNullValue()));
		assertThat(encryptedBundle, is(not(bundle)));
		assertThat(encryptedBundle.size(), is(3));
		assertThat(
				encryptedBundle.getString(TestCryptography.encryptToString(keyFirst)),
				is(TestCryptography.encryptToString(dataFirst))
		);
		assertThat(
				encryptedBundle.getString(TestCryptography.encryptToString(keySecond)),
				is(TestCryptography.encryptToString(dataSecond))
		);
		assertThat(encryptedBundle.getString(keyThird), is(nullValue()));
		assertThat(bundle.size(), is(3));
		assertThat(bundle.getString(keyFirst), is(dataFirst));
		assertThat(bundle.getString(keySecond), is(dataSecond));
		assertThat(bundle.getString(keyThird), is(nullValue()));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(keyFirst));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(keySecond));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(keyThird));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataFirst));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataSecond));
		verifyNoMoreInteractions(mockKeyEncrypto, mockCrypto);
	}

	@Test public void testEncryptBundleWithoutEncryption() {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final String keyFirst = "KEY.First";
		final String keySecond = "KEY.Second";
		final String keyThird = "KEY.Third";
		final String dataFirst = "DATA.First";
		final String dataSecond = "DATA.Second";
		final Bundle bundle = new Bundle();
		bundle.putString(keyFirst, dataFirst);
		bundle.putString(keySecond, dataSecond);
		bundle.putString(keyThird, null);
		// Act:
		final Bundle encryptedBundle = manager.encryptBundle(bundle);
		// Assert:
		assertThat(encryptedBundle, is(notNullValue()));
		assertThat(encryptedBundle, is(not(bundle)));
		assertThat(encryptedBundle.size(), is(3));
		assertThat(encryptedBundle.getString(keyFirst), is(dataFirst));
		assertThat(encryptedBundle.getString(keySecond), is(dataSecond));
		assertThat(encryptedBundle.getString(keyThird), is(nullValue()));
		assertThat(bundle.size(), is(3));
		assertThat(bundle.getString(keyFirst), is(dataFirst));
		assertThat(bundle.getString(keySecond), is(dataSecond));
		assertThat(bundle.getString(keyThird), is(nullValue()));
	}

	@Test public void testEncryptBundleNullOrEmpty() {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		final Crypto mockCrypto = mock(Crypto.class);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		// Act + Assert:
		assertThat(manager.encryptBundle(null), is(nullValue()));
		assertThat(manager.encryptBundle(Bundle.EMPTY), is(Bundle.EMPTY));
		verifyNoMoreInteractions(mockKeyEncrypto, mockCrypto);
	}

	@Test public void testKeyEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final String key = "KEY";
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(key))).thenReturn(TestCryptography.encrypt(key));
		final String keyEncrypted = CryptographyUtils.encrypt(key, mockKeyEncrypto);
		final Crypto mockCrypto = mock(Crypto.class);
		manager.setCrypto(mockCrypto);
		Mockito.clearInvocations(mockKeyEncrypto);
		// Act + Assert:
		assertThat(manager.encryptKey(key), is(key));
		manager.setKeyEncrypto(mockKeyEncrypto);
		assertThat(manager.encryptKey(key), is(keyEncrypted));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(key));
		verifyNoMoreInteractions(mockKeyEncrypto);
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		manager.setKeyEncrypto(mockKeyEncrypto);
		final String data = "DATA";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(data))).thenReturn(TestCryptography.encrypt(data));
		final String dataEncrypted = CryptographyUtils.encrypt(data, mockCrypto);
		Mockito.clearInvocations(mockCrypto);
		// Act + Assert:
		assertThat(manager.encrypt(data), is(data));
		manager.setCrypto(mockCrypto);
		assertThat(manager.encrypt(data), is(dataEncrypted));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(data));
		verifyNoMoreInteractions(mockCrypto);
		verifyNoMoreInteractions(mockKeyEncrypto);
	}

	@Test public void testDecryption() throws UnsupportedEncodingException {
		// Arrange:
		final BaseAccountManager<TestAccount> manager = new TestManager(context());
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		manager.setKeyEncrypto(mockKeyEncrypto);
		final String data = "DATA";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(data))).thenReturn(TestCryptography.encrypt(data));
		final String dataEncrypted = TestCryptography.encryptToString(data);
		final byte[] dataEncryptedDecoded = TestCryptography.decode(dataEncrypted);
		when(mockCrypto.decrypt(dataEncryptedDecoded)).thenReturn(TestCryptography.bytesOf(data));
		Mockito.clearInvocations(mockCrypto);
		// Act + Assert:
		assertThat(manager.decrypt(data), is(data));
		manager.setCrypto(mockCrypto);
		assertThat(manager.decrypt(dataEncrypted), is(data));
		verify(mockCrypto).decrypt(dataEncryptedDecoded);
		verifyNoMoreInteractions(mockCrypto);
		verifyNoMoreInteractions(mockKeyEncrypto);
	}

	@Test public void testAddAccount() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		account.setPassword("1234");
		final Bundle dataBundle = new Bundle();
		account.setDataBundle(dataBundle);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		when(mockSystemManager.addAccountExplicitly(any(Account.class), anyString(), any(Bundle.class))).thenReturn(true);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.addAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).addAccountExplicitly(any(Account.class), eq(account.getPassword()), eq(dataBundle));
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAddAccountWithAuthTokens() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		account.setOAuthToken("token");
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		when(mockSystemManager.addAccountExplicitly(any(Account.class), eq(null), eq(null))).thenReturn(true);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.addAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).addAccountExplicitly(any(Account.class), eq(null), eq(null));
		verify(mockSystemManager).setAuthToken(any(Account.class), eq(TestAccount.TOKEN_TYPE_O_AUTH), eq(account.getOAuthToken()));
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAddAccountThatAlreadyExists() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final Account systemAccount = new Account(account.getName(), TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.addAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAddAccountWithSystemOperationFailure() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		when(mockSystemManager.addAccountExplicitly(any(Account.class), anyString(), any(Bundle.class))).thenReturn(false);
		when(mockSystemManager.addAccountExplicitly(any(Account.class), eq(null), eq(null))).thenReturn(false);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.addAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isFailure(), is(true));
		assertThat(result.getError().code(), is(BaseAccountManager.ERROR_SYSTEM_OPERATION_FAILURE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).addAccountExplicitly(any(Account.class), eq(null), eq(null));
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testUpdateAccount() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		account.setPassword("1234");
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.updateAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).setPassword(systemAccount, account.getPassword());
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testUpdateAccountWithDataAndAuthTokens() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final Bundle dataBundle = new Bundle();
		dataBundle.putString("KEY.Name", "Test User");
		account.setDataBundle(dataBundle);
		account.setOAuthToken("token");
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.updateAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).setPassword(systemAccount, null);
		verify(mockSystemManager).setAuthToken(systemAccount, TestAccount.TOKEN_TYPE_O_AUTH, account.getOAuthToken());
		verify(mockSystemManager).setUserData(systemAccount, "KEY.Name", "Test User");
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testUpdateAccountThatDoesNotExist() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.updateAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isFailure(), is(true));
		assertThat(result.getError().code(), is(BaseAccountManager.ERROR_ACCOUNT_DOES_NOT_EXIST));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@SuppressWarnings("unchecked")
	@Config(sdk = Build.VERSION_CODES.M)
	@Test public void testRemoveAccountOnMarshmallow() throws AuthenticatorException, OperationCanceledException, IOException {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final AccountManagerFuture<Bundle> mockFeature = (AccountManagerFuture<Bundle>) mock(AccountManagerFuture.class);
		when(mockFeature.getResult()).thenReturn(Bundle.EMPTY);
		when(mockSystemManager.removeAccount(systemAccount, null, null, null)).thenReturn(mockFeature);
		when(mockSystemManager.peekAuthToken(systemAccount, TestAccount.TOKEN_TYPE_O_AUTH)).thenReturn("token");
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.removeAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).removeAccount(systemAccount, null, null, null);
		verify(mockSystemManager).setPassword(systemAccount, null);
		verify(mockSystemManager).peekAuthToken(systemAccount, TestAccount.TOKEN_TYPE_O_AUTH);
		verify(mockSystemManager).invalidateAuthToken(systemAccount.type, "token");
		verifyNoMoreInteractions(mockSystemManager);
	}

	@SuppressWarnings("unchecked")
	@Config(sdk = Build.VERSION_CODES.LOLLIPOP)
	@Test public void testRemoveAccountOnLollipop() throws AuthenticatorException, OperationCanceledException, IOException {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final AccountManagerFuture<Boolean> mockFeature = (AccountManagerFuture<Boolean>) mock(AccountManagerFuture.class);
		when(mockFeature.getResult()).thenReturn(true);
		when(mockSystemManager.removeAccount(systemAccount, null, null)).thenReturn(mockFeature);
		when(mockSystemManager.peekAuthToken(systemAccount, TestAccount.TOKEN_TYPE_O_AUTH)).thenReturn("token");
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.removeAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isSuccess(), is(true));
		assertThat(result.getError(), is(AccountError.NONE));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).removeAccount(systemAccount, null, null);
		verify(mockSystemManager).setPassword(systemAccount, null);
		verify(mockSystemManager).peekAuthToken(systemAccount, TestAccount.TOKEN_TYPE_O_AUTH);
		verify(mockSystemManager).invalidateAuthToken(systemAccount.type, "token");
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testRemoveAccountThatDoesNotExist() {
		// Arrange:
		final TestAccount account = new TestAccount("user@account.test");
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final AccountResult result = manager.removeAccount(account);
		// Assert:
		assertThat(result, is(notNullValue()));
		assertThat(result.isFailure(), is(true));
		assertThat(result.getError().code(), is(BaseAccountManager.ERROR_ACCOUNT_DOES_NOT_EXIST));
		verify(mockSystemManager).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testIsAccountAuthenticated() {
		// Arrange:
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.peekAuthToken(systemAccount, tokenTypeOAuth)).thenReturn("1234567890");
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		assertThat(manager.isAccountAuthenticated(systemAccount.name, tokenTypeOAuth), is(true));
		assertThat(manager.isAccountAuthenticated(systemAccount.name, tokenTypeSecondary), is(false));
		verify(mockSystemManager, times(2)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).peekAuthToken(systemAccount, tokenTypeOAuth);
		verify(mockSystemManager).peekAuthToken(systemAccount, tokenTypeSecondary);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testIsAccountAuthenticatedWithEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeOAuth))).thenReturn(TestCryptography.encrypt(tokenTypeOAuth));
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeSecondary))).thenReturn(TestCryptography.encrypt(tokenTypeSecondary));
		final String token = "1234567890";
		final Crypto mockCrypto = mock(Crypto.class);
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth))).thenReturn(
				TestCryptography.encryptToString(token)
		);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		// Act + Assert:
		assertThat(manager.isAccountAuthenticated(systemAccount.name, tokenTypeOAuth), is(true));
		assertThat(manager.isAccountAuthenticated(systemAccount.name, tokenTypeSecondary), is(false));
		verify(mockSystemManager, times(2)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(tokenTypeOAuth));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(tokenTypeSecondary));
		verify(mockSystemManager).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth));
		verify(mockSystemManager).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeSecondary));
		verifyNoMoreInteractions(mockSystemManager, mockKeyEncrypto);
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testAccountAuthToken() {
		// Arrange:
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		final String token = "1234567890";
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(nullValue()));
		when(mockSystemManager.peekAuthToken(systemAccount, tokenTypeOAuth)).thenReturn(token);
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(token));
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeSecondary), is(nullValue()));
		verify(mockSystemManager, times(3)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager, times(2)).peekAuthToken(systemAccount, tokenTypeOAuth);
		verify(mockSystemManager).peekAuthToken(systemAccount, tokenTypeSecondary);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAccountAuthTokenWithEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeOAuth))).thenReturn(TestCryptography.encrypt(tokenTypeOAuth));
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeSecondary))).thenReturn(TestCryptography.encrypt(tokenTypeSecondary));
		final String token = "1234567890";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(token)))).thenReturn(
				TestCryptography.bytesOf(token)
		);
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		// Act + Assert:
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(nullValue()));
		when(mockSystemManager.peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth))).thenReturn(TestCryptography.encryptToString(token));
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(token));
		assertThat(manager.peekAccountAuthToken(systemAccount.name, tokenTypeSecondary), is(nullValue()));
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(tokenTypeOAuth));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(tokenTypeSecondary));
		verify(mockCrypto).decrypt(TestCryptography.decode(TestCryptography.encryptToString(token)));
		verify(mockSystemManager, times(3)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager, times(2)).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth));
		verify(mockSystemManager).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeSecondary));
		verifyNoMoreInteractions(mockSystemManager, mockKeyEncrypto, mockCrypto);
	}

	@Test public void testInvalidateAccountAuthToken() {
		// Arrange:
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		final String token = "1234567890";
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(false));
		when(mockSystemManager.peekAuthToken(systemAccount, tokenTypeOAuth)).thenReturn(token);
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(true));
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeSecondary), is(false));
		verify(mockSystemManager, times(3)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager, times(2)).peekAuthToken(systemAccount, tokenTypeOAuth);
		verify(mockSystemManager).peekAuthToken(systemAccount, tokenTypeSecondary);
		verify(mockSystemManager).invalidateAuthToken(systemAccount.type, token);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testInvalidateAccountAuthTokenWithEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		final String tokenTypeOAuth = TestAccount.TOKEN_TYPE_O_AUTH;
		final String tokenTypeSecondary = "auth_secondary";
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeOAuth))).thenReturn(TestCryptography.encrypt(tokenTypeOAuth));
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(tokenTypeSecondary))).thenReturn(TestCryptography.encrypt(tokenTypeSecondary));
		final String token = "1234567890";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(token)))).thenReturn(
				TestCryptography.bytesOf(token)
		);
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		// Act + Assert:
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(false));
		when(mockSystemManager.peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth))).thenReturn(token);
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeOAuth), is(true));
		assertThat(manager.invalidateAccountAuthToken(systemAccount.name, tokenTypeSecondary), is(false));
		verify(mockSystemManager, times(3)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(tokenTypeOAuth));
		verify(mockKeyEncrypto).encrypt(TestCryptography.bytesOf(tokenTypeSecondary));
		verify(mockSystemManager, times(2)).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeOAuth));
		verify(mockSystemManager).peekAuthToken(systemAccount, TestCryptography.encryptToString(tokenTypeSecondary));
		verify(mockSystemManager).invalidateAuthToken(systemAccount.type, token);
		verifyNoMoreInteractions(mockSystemManager, mockKeyEncrypto);
		verifyNoMoreInteractions(mockCrypto);
	}

	@Test public void testAccountData() {
		// Arrange:
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.getUserData(systemAccount, "DATA.FirstName")).thenReturn("First Name");
		when(mockSystemManager.getUserData(systemAccount, "DATA.LastName")).thenReturn("Last Name");
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		manager.setAccountData(systemAccount.name, "DATA.FirstName", "First Name");
		manager.setAccountData(systemAccount.name, "DATA.LastName", "Last Name");
		assertThat(manager.getAccountData(systemAccount.name, "DATA.FirstName"), is("First Name"));
		assertThat(manager.getAccountData(systemAccount.name, "DATA.LastName"), is("Last Name"));
		verify(mockSystemManager, times(4)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).setUserData(systemAccount, "DATA.FirstName", "First Name");
		verify(mockSystemManager).setUserData(systemAccount, "DATA.LastName", "Last Name");
		verify(mockSystemManager).getUserData(systemAccount, "DATA.FirstName");
		verify(mockSystemManager).getUserData(systemAccount, "DATA.LastName");
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAccountDataWithEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final String keyFirstName = "DATA.FirstName";
		final String keyLastName = "DATA.LastName";
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyFirstName))).thenReturn(TestCryptography.encrypt(keyFirstName));
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyLastName))).thenReturn(TestCryptography.encrypt(keyLastName));
		final String dataFirstName = "First Name";
		final String dataLastName = "Last Name";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataFirstName))).thenReturn(TestCryptography.encrypt(dataFirstName));
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataLastName))).thenReturn(TestCryptography.encrypt(dataLastName));
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataFirstName)))).thenReturn(
				TestCryptography.bytesOf(dataFirstName)
		);
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataLastName)))).thenReturn(
				TestCryptography.bytesOf(dataLastName)
		);
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.getUserData(systemAccount, TestCryptography.encryptToString(keyFirstName))).thenReturn(
				TestCryptography.encryptToString(dataFirstName)
		);
		when(mockSystemManager.getUserData(systemAccount, TestCryptography.encryptToString(keyLastName))).thenReturn(
				TestCryptography.encryptToString(dataLastName)
		);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		Mockito.clearInvocations(mockCrypto);
		// Act + Assert:
		manager.setAccountData(systemAccount.name, keyFirstName, dataFirstName);
		manager.setAccountData(systemAccount.name, keyLastName, dataLastName);
		assertThat(manager.getAccountData(systemAccount.name, keyFirstName), is(dataFirstName));
		assertThat(manager.getAccountData(systemAccount.name, keyLastName), is(dataLastName));
		verify(mockSystemManager, times(4)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(keyFirstName));
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(keyLastName));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataFirstName));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataLastName));
		verify(mockCrypto).decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataFirstName)));
		verify(mockCrypto).decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataLastName)));
		verify(mockSystemManager).setUserData(systemAccount, TestCryptography.encryptToString(keyFirstName), TestCryptography.encryptToString(dataFirstName));
		verify(mockSystemManager).setUserData(systemAccount, TestCryptography.encryptToString(keyLastName), TestCryptography.encryptToString(dataLastName));
		verify(mockSystemManager).getUserData(systemAccount, TestCryptography.encryptToString(keyFirstName));
		verify(mockSystemManager).getUserData(systemAccount, TestCryptography.encryptToString(keyLastName));
		verifyNoMoreInteractions(mockSystemManager, mockKeyEncrypto, mockCrypto);
	}

	@Test public void testAccountDataForAccountThatDoesNotExist() {
		// Arrange:
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		manager.setAccountData("user@account.test", "DATA.FirstName", "First Name");
		manager.setAccountData("user@account.test", "DATA.LastName", "Last Name");
		assertThat(manager.getAccountData("user@account.test", "DATA.FirstName"), is(nullValue()));
		assertThat(manager.getAccountData("user@account.test", "DATA.LastName"), is(nullValue()));
		verify(mockSystemManager, times(4)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAccountDataBundle() {
		// Arrange:
		final Bundle dataBundle = new Bundle();
		dataBundle.putString("DATA.FirstName", "First Name");
		dataBundle.putString("DATA.LastName", "Last Name");
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.getUserData(systemAccount, "DATA.FirstName")).thenReturn("First Name");
		when(mockSystemManager.getUserData(systemAccount, "DATA.LastName")).thenReturn("Last Name");
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		manager.setAccountDataBundle(systemAccount.name, dataBundle);
		final Bundle persistedDataBundle = manager.getAccountDataBundle(systemAccount.name, new String[]{"DATA.FirstName", "DATA.LastName"});
		assertThat(persistedDataBundle, is(notNullValue()));
		assertThat(persistedDataBundle.getString("DATA.FirstName"), is("First Name"));
		assertThat(persistedDataBundle.getString("DATA.LastName"), is("Last Name"));
		verify(mockSystemManager, times(2)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockSystemManager).setUserData(systemAccount, "DATA.FirstName", "First Name");
		verify(mockSystemManager).setUserData(systemAccount, "DATA.LastName", "Last Name");
		verify(mockSystemManager).getUserData(systemAccount, "DATA.FirstName");
		verify(mockSystemManager).getUserData(systemAccount, "DATA.LastName");
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testAccountDataBundleWithEncryption() throws UnsupportedEncodingException {
		// Arrange:
		final String keyFirstName = "DATA.FirstName";
		final String keyLastName = "DATA.LastName";
		final Encrypto mockKeyEncrypto = mock(Encrypto.class);
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyFirstName))).thenReturn(TestCryptography.encrypt(keyFirstName));
		when(mockKeyEncrypto.encrypt(TestCryptography.bytesOf(keyLastName))).thenReturn(TestCryptography.encrypt(keyLastName));
		final String dataFirstName = "First Name";
		final String dataLastName = "Last Name";
		final Crypto mockCrypto = mock(Crypto.class);
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataFirstName))).thenReturn(TestCryptography.encrypt(dataFirstName));
		when(mockCrypto.encrypt(TestCryptography.bytesOf(dataLastName))).thenReturn(TestCryptography.encrypt(dataLastName));
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataFirstName)))).thenReturn(
				TestCryptography.bytesOf(dataFirstName)
		);
		when(mockCrypto.decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataLastName)))).thenReturn(
				TestCryptography.bytesOf(dataLastName)
		);
		final Bundle dataBundle = new Bundle();
		dataBundle.putString(keyFirstName, dataFirstName);
		dataBundle.putString(keyLastName, dataLastName);
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{systemAccount});
		when(mockSystemManager.getUserData(systemAccount, TestCryptography.encryptToString(keyFirstName))).thenReturn(
				TestCryptography.encryptToString(dataFirstName)
		);
		when(mockSystemManager.getUserData(systemAccount, TestCryptography.encryptToString(keyLastName))).thenReturn(
				TestCryptography.encryptToString(dataLastName)
		);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		manager.setKeyEncrypto(mockKeyEncrypto);
		manager.setCrypto(mockCrypto);
		Mockito.clearInvocations(mockCrypto);
		// Act + Assert:
		manager.setAccountDataBundle(systemAccount.name, dataBundle);
		final Bundle persistedDataBundle = manager.getAccountDataBundle(systemAccount.name, new String[]{keyFirstName, keyLastName});
		assertThat(persistedDataBundle, is(notNullValue()));
		assertThat(persistedDataBundle.getString(keyFirstName), is(dataFirstName));
		assertThat(persistedDataBundle.getString(keyLastName), is(dataLastName));
		verify(mockSystemManager, times(2)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(keyFirstName));
		verify(mockKeyEncrypto, times(2)).encrypt(TestCryptography.bytesOf(keyLastName));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataFirstName));
		verify(mockCrypto).encrypt(TestCryptography.bytesOf(dataLastName));
		verify(mockCrypto).decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataFirstName)));
		verify(mockCrypto).decrypt(TestCryptography.decode(TestCryptography.encryptToString(dataLastName)));
		verify(mockSystemManager).setUserData(systemAccount, TestCryptography.encryptToString(keyFirstName), TestCryptography.encryptToString(dataFirstName));
		verify(mockSystemManager).setUserData(systemAccount, TestCryptography.encryptToString(keyLastName), TestCryptography.encryptToString(dataLastName));
		verify(mockSystemManager).getUserData(systemAccount, TestCryptography.encryptToString(keyFirstName));
		verify(mockSystemManager).getUserData(systemAccount, TestCryptography.encryptToString(keyLastName));
		verifyNoMoreInteractions(mockSystemManager, mockKeyEncrypto, mockCrypto);
	}

	@Test public void testAccountDataBundleForAccountThatDoesNotExist() {
		// Arrange:
		final Bundle dataBundle = new Bundle();
		dataBundle.putString("DATA.FirstName", "First Name");
		dataBundle.putString("DATA.LastName", "Last Name");
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act + Assert:
		manager.setAccountDataBundle("user@account.test", dataBundle);
		final Bundle persistedDataBundle = manager.getAccountDataBundle("user@account.test", new String[]{"DATA.FirstName", "DATA.LastName"});
		assertThat(persistedDataBundle, is(nullValue()));
		verify(mockSystemManager, times(2)).getAccountsByType(TestManager.ACCOUNT_TYPE);
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testFindAccount() {
		// Arrange:
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{
				systemAccount,
				new Account("Second Account", TestManager.ACCOUNT_TYPE),
				new Account("Third Account", TestManager.ACCOUNT_TYPE)
		});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final TestAccount account = manager.findAccount(systemAccount.name);
		// Assert:
		assertThat(account, is(notNullValue()));
		assertThat(account.getName(), is(systemAccount.name));
		assertThat(account.getPassword(), is(nullValue()));
		assertThat(account.getOAuthToken(), is(nullValue()));
		assertThat(account.getDataBundle(), is(nullValue()));
		verify(mockSystemManager).getAccountsByType(manager.getAccountType());
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testFindAccountThatDoesNotExist() {
		// Arrange:
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final TestAccount account = manager.findAccount("user@account.test");
		// Assert:
		assertThat(account, is(nullValue()));
		verify(mockSystemManager).getAccountsByType(manager.getAccountType());
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testFindSystemAccount() {
		// Arrange:
		final Account systemAccount = new Account("user@account.test", TestManager.ACCOUNT_TYPE);
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(TestManager.ACCOUNT_TYPE)).thenReturn(new Account[]{
				systemAccount,
				new Account("Second Account", TestManager.ACCOUNT_TYPE),
				new Account("Third Account", TestManager.ACCOUNT_TYPE)
		});
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final Account account = manager.findSystemAccount(systemAccount.name);
		// Assert:
		assertThat(account, is(systemAccount));
		verify(mockSystemManager).getAccountsByType(manager.getAccountType());
		verifyNoMoreInteractions(mockSystemManager);
	}

	@Test public void testFindSystemAccountThatDoesNotExist() {
		// Arrange:
		final AccountManager mockSystemManager = mock(AccountManager.class);
		when(mockSystemManager.getAccountsByType(anyString())).thenReturn(new Account[0]);
		final BaseAccountManager<TestAccount> manager = new TestManager(mockSystemManager);
		// Act:
		final Account account = manager.findSystemAccount("user@account.test");
		// Assert:
		assertThat(account, is(nullValue()));
		verify(mockSystemManager).getAccountsByType(manager.getAccountType());
		verifyNoMoreInteractions(mockSystemManager);
	}

	private class TestAccount extends BaseAccount {

		TestAccount(@NonNull String name) { this(name, null); }
		TestAccount(@NonNull String name, @Nullable String password) { super(name, password); }
	}

	private interface TestObserver extends BaseAccountManager.AccountObserver<TestAccount> {}

	private class TestManager extends BaseAccountManager<TestAccount> {

		static final String ACCOUNT_TYPE = "account.test";

		TestManager(@NonNull Context context) { super(context, ACCOUNT_TYPE); }
		TestManager(final AccountManager systemManager) { super(systemManager, ACCOUNT_TYPE); }

		@Override @NonNull protected TestAccount onCreateAccountInstance(
				@NonNull final AccountManager systemManager,
				@NonNull final Account systemAccount) {
			return new TestAccount(systemAccount.name);
		}
	}
}