/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Martin Albedinsky
 */
public class AccountErrorsTest extends TestCase {

	@Test(expected = IllegalAccessException.class)
	public void testInstantiation() throws Exception {
		// Act:
		AccountErrors.class.newInstance();
	}

	@Test(expected = InvocationTargetException.class)
	public void testInstantiationWithAccessibleConstructor() throws Exception {
		// Arrange:
		final Constructor<AccountErrors> constructor = AccountErrors.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		// Act:
		constructor.newInstance();
	}

	@Test public void testCreateWithCodeOnly() {
		// Act:
		final AccountError error = AccountErrors.create("ERROR");
		// Assert:
		assertThat(error, is(notNullValue()));
		assertThat(error.code(), is("ERROR"));
		assertThat(error.cause(), is(AccountError.NONE.cause()));
		assertThat(error.toString(), is("AccountError(code: ERROR, cause: NONE)"));
	}

	@Test public void testCreateWithCodeAndCause() {
		// Arrange:
		final Throwable cause = new IllegalStateException();
		// Act:
		final AccountError error = AccountErrors.create("ERROR", cause);
		// Assert:
		assertThat(error, is(notNullValue()));
		assertThat(error.code(), is("ERROR"));
		assertThat(error.cause(), is(cause));
		assertThat(error.toString(), is("AccountError(code: ERROR, cause: java.lang.IllegalStateException)"));
	}
}