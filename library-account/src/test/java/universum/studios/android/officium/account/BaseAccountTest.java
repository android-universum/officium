/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.os.Bundle;

import org.junit.Test;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * @author Martin Albedinsky
 */
public final class BaseAccountTest extends AndroidTestCase {

	private static final String NAME = "user@name.com";
	private static final String PASSWORD = "pass";

    @Test public void testContract() {
    	// Assert:
		assertThat(BaseAccount.TOKEN_TYPE_O_AUTH, is("oAuth"));
		assertThat(BaseAccount.AUTH_TOKEN_TYPES, is(new String[]{BaseAccount.TOKEN_TYPE_O_AUTH}));
    }

    @Test public void testInstantiationWithName() {
    	// Act:
    	final BaseAccount account = new TestAccount(NAME);
	    // Assert:
	    assertThat(account.getName(), is(NAME));
    	assertThat(account.getPassword(), is(nullValue()));
    	assertThat(account.getDataBundle(), is(nullValue()));
    }

    @Test public void testInstantiationWithNameAndPassword() {
    	// Act:
    	final BaseAccount account = new TestAccount(NAME, PASSWORD);
	    // Assert:
    	assertThat(account.getName(), is(NAME));
    	assertThat(account.getPassword(), is(PASSWORD));
    	assertThat(account.getDataBundle(), is(nullValue()));
    }

	@Test public void testPassword() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		account.setPassword("123456");
		assertThat(account.getPassword(), is("123456"));
		account.setPassword("1234");
		assertThat(account.getPassword(), is("1234"));
	}

	@Test public void testGetAuthTokenTypes() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.getAuthTokenTypes(), is(BaseAccount.AUTH_TOKEN_TYPES));
	}

	@Test public void testOAuthToken() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.getOAuthToken(), is(nullValue()));
		account.setOAuthToken("123456789");
		account.setOAuthToken("1234567890");
		assertThat(account.getOAuthToken(), is("1234567890"));
		final Map<String, String> authTokens = account.getAuthTokens();
		assertThat(authTokens, is(notNullValue()));
		assertThat(authTokens.size(), is(1));
		assertThat(authTokens.get(BaseAccount.TOKEN_TYPE_O_AUTH), is("1234567890"));
		account.setOAuthToken(null);
		assertThat(account.getOAuthToken(), is(nullValue()));
		assertThat(authTokens.isEmpty(), is(true));
	}

	@Test public void testAuthToken() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		account.putAuthToken(BaseAccount.TOKEN_TYPE_O_AUTH, "123456789");
		account.putAuthToken("authToken:1", "123456789");
		account.putAuthToken("authToken:2", "1234567890");
		account.removeAuthToken("authToken:1");
		final Map<String, String> authTokens = account.getAuthTokens();
		assertThat(authTokens, is(notNullValue()));
		assertThat(authTokens.size(), is(2));
		assertThat(authTokens.get(BaseAccount.TOKEN_TYPE_O_AUTH), is("123456789"));
		assertThat(authTokens.get("authToken:2"), is("1234567890"));
	}

	@Test public void testRemoveAuthTokenWhenEmpty() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		account.removeAuthToken("authToken:1");
		assertThat(account.getAuthTokens(), is(nullValue()));
	}

	@Test public void testHasAuthToken() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.hasAuthToken(BaseAccount.TOKEN_TYPE_O_AUTH), is(false));
		account.putAuthToken(BaseAccount.TOKEN_TYPE_O_AUTH, "0");
		account.putAuthToken("authToken:1", "1");
		account.putAuthToken("authToken:2", "2");
		account.putAuthToken("authToken:3", "3");
		account.removeAuthToken("authToken:2");
		assertThat(account.hasAuthToken(BaseAccount.TOKEN_TYPE_O_AUTH), is(true));
		assertThat(account.hasAuthToken("authToken:1"), is(true));
		assertThat(account.hasAuthToken("authToken:2"), is(false));
		assertThat(account.hasAuthToken("authToken:3"), is(true));
	}

	@Test public void testGetAuthTokensDefault() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.getAuthTokens(), is(nullValue()));
	}

	@Test public void testData() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.getData("data:1"), is(nullValue()));
		account.putData("data:1", "Data 1");
		account.putData("data:2", "Data 2");
		account.putData("data:3", "Data 3");
		final Bundle dataBundle = account.getDataBundle();
		assertThat(dataBundle, is(notNullValue()));
		assertThat(dataBundle.size(), is(3));
		assertThat(account.getData("data:1"), is("Data 1"));
		assertThat(account.getData("data:1"), is(dataBundle.get("data:1")));
		assertThat(account.getData("data:2"), is("Data 2"));
		assertThat(account.getData("data:2"), is(dataBundle.get("data:2")));
		assertThat(account.getData("data:3"), is("Data 3"));
		assertThat(account.getData("data:3"), is(dataBundle.get("data:3")));
	}

	@Test public void testHasData() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		assertThat(account.hasData("data:1"), is(false));
		assertThat(account.hasData("data:2"), is(false));
		account.putData("data:1", "Data 1");
		account.putData("data:2", null);
		assertThat(account.hasData("data:1"), is(true));
		assertThat(account.hasData("data:2"), is(true));
    }

	@Test public void testRemoveData() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
		account.removeData("data:1");
		account.putData("data:1", "Data 1");
		account.putData("data:2", null);
		account.removeData("data:1");
		account.removeData("data:2");
		account.removeData("data:3");
		assertThat(account.hasData("data:1"), is(false));
		assertThat(account.hasData("data:2"), is(false));
    }

	@Test public void testDataBundle() {
		// Arrange:
		final BaseAccount account = new TestAccount(NAME);
		// Act + Assert:
    	final Bundle dataBundle = new Bundle();
    	dataBundle.putString("data:1", "Data 1");
    	dataBundle.putString("data:2", "Data 2");
    	dataBundle.putInt("data:int", 1);
    	account.setDataBundle(dataBundle);
    	assertThat(account.getDataBundle(), is(dataBundle));
    	assertThat(account.getData("data:1"), is("Data 1"));
    	assertThat(account.getData("data:2"), is("Data 2"));
    	assertThat(account.getData("data:int"), is(nullValue()));
	}

	@Test public void testHashCode() {
		// Arrange + Act + Assert:
    	assertThat(new TestAccount(NAME).hashCode(), is(NAME.hashCode()));
		assertThat(new TestAccount(NAME, PASSWORD).hashCode(), is(NAME.hashCode()));
	}

	@SuppressWarnings({"ObjectEqualsNull", "EqualsBetweenInconvertibleTypes", "EqualsWithItself", "ConstantConditions"})
	@Test public void testEquals() {
    	// Arrange:
    	final BaseAccount account = new TestAccount(NAME);
    	// Act + Assert:
    	assertThat(account.equals(account), is(true));
    	assertThat(new TestAccount(NAME).equals(new TestAccount(NAME)), is(true));
    	assertThat(new TestAccount(NAME, PASSWORD).equals(new TestAccount(NAME)), is(true));
    	assertThat(new TestAccount(NAME).equals(new TestAccount("user2@name.com")), is(false));
    	assertThat(new TestAccount(NAME).equals(null), is(false));
    	assertThat(new TestAccount(NAME).equals(0), is(false));
	}

	@Test public void testToString() {
    	// Arrange + Act + Assert:
		assertThat(new TestAccount(NAME).toString(), is("TestAccount(name: " + NAME + ")"));
		assertThat(new TestAccount(NAME, PASSWORD).toString(), is("TestAccount(name: " + NAME + ")"));
	}
	
	private class TestAccount extends BaseAccount {

		TestAccount(@NonNull final String name) { super(name); }
		TestAccount(@NonNull final String name, @Nullable final String password) { super(name, password); }
	}
}