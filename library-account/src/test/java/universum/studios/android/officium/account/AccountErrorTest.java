/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import org.junit.Test;

import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Martin Albedinsky
 */
public class AccountErrorTest extends TestCase {

	@Test public void testNoneInstance() {
		// Assert:
		assertThat(AccountError.NONE, is(notNullValue()));
		assertThat(AccountError.NONE.code(), is("NONE"));
		assertThat(AccountError.NONE.cause(), is(notNullValue()));
		assertThat(AccountError.NONE.toString(), is("NONE"));
	}

	@Test public void testUnknownInstance() {
		// Assert:
		assertThat(AccountError.UNKNOWN, is(notNullValue()));
		assertThat(AccountError.UNKNOWN.code(), is("UNKNOWN"));
		assertThat(AccountError.UNKNOWN.cause(), is(AccountError.NONE.cause()));
		assertThat(AccountError.UNKNOWN.toString(), is("UNKNOWN"));
	}
}