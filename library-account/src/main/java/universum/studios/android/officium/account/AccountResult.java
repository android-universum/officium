/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * Interface describing result of an account related operation.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface AccountResult {

	/*
	 * Constants ===================================================================================
	 */

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Checks whether this account result is a success or not.
	 *
	 * @return {@code True} if this account result represents a success of which result value may be
	 * obtained via {@code #getValue()}, {@code false} if it is actually a failure.
	 *
	 * @see #isFailure()
	 */
	boolean isSuccess();

	/**
	 * Returns the value of this successful account result.
	 * <p>
	 * If this account result is a failure, the returned value should be a simple `EMPTY` value.
	 *
	 * @return Value associated with this account result.
	 *
	 * @see #isSuccess()
	 */
	@NonNull Bundle getValue();

	/**
	 * Checks whether this account result is a failure or not.
	 *
	 * @return {@code True} if this account result represents a failure of which error may be
	 * obtained via {@link #getError()}, {@code false} if it is actually a success.
	 *
	 * @see #isSuccess()
	 */
	boolean isFailure();

	/**
	 * Returns the error due to which value of this account result could not be produced.
	 * <p>
	 * If this result is a successful one, the returned error should be a simple `NONE` error.
	 *
	 * @return Error associated with this account result.
	 *
	 * @see #isFailure()
	 */
	@NonNull AccountError getError();
}