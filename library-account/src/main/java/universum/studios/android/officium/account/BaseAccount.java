/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Class representing a base for application accounts which can provide some basic information about
 * user. Each instance of BaseAccount must be created with <var>name</var> via {@link BaseAccount(String)}
 * and optionally also with <var>password</var> via {@link BaseAccount(String, String)}.
 * <p>
 * There can be specified also additional data for each account instance either via {@link #putData(String, String)}
 * or {@link #setDataBundle(Bundle)} including authentication tokens via {@link #putAuthToken(String, String)}.
 * <p>
 * <b>Note</b> that {@link #getAuthTokenTypes()} method should always return array of token types
 * that are specific for a particular type of BaseAccount implementation. This array is used by
 * {@link BaseAccountManager} to iterate through {@link #getAuthTokens() types map} and obtain all
 * token data from it and persist it for the associated {@link Account system account} via
 * {@link AccountManager#setAuthToken(Account, String, String)}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public abstract class BaseAccount {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseAccount";

	/**
	 * Type key for the <b>OAuth</b> token.
	 *
	 * @see #putAuthToken(String, String)
	 */
	public static final String TOKEN_TYPE_O_AUTH = "oAuth";

	/**
	 * Array specifying set of default authentication token types.
	 * <p>
	 * Contains single {@link #TOKEN_TYPE_O_AUTH} key.
	 */
	protected static final String[] AUTH_TOKEN_TYPES = {TOKEN_TYPE_O_AUTH};

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Name specified for this account.
	 */
	@NonNull private final String name;

	/**
	 * Password specified for this account. May be {@code null}.
	 *
	 * @see #setPassword(String)
	 */
	@Nullable private String password;

	/**
	 * Map with authentication tokens for this account mapped to their types.
	 *
	 * @see #putAuthToken(String, String)
	 */
	@Nullable private Map<String, String> authTokens;

	/**
	 * Bundle with data for this account.
	 *
	 * @see #setDataBundle(Bundle)
	 * @see #putData(String, String)
	 */
	@Nullable private Bundle dataBundle;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #BaseAccount(String, String)} with {@code null} password.
	 *
	 * @param name The desired name for the new account.
	 */
	protected BaseAccount(@NonNull final String name) {
		this(name, null);
	}

	/**
	 * Creates a new instance of BaseAccount with the specified <var>name</var> and <var>password</var>.
	 *
	 * @param name     The desired name for the new account.
	 * @param password The desired password for the new account. May be {@code null}.
	 */
	protected BaseAccount(@NonNull final String name, @Nullable final String password) {
		this.name = name;
		this.password = password;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the name specified for this account.
	 *
	 * @return This account's name.
	 *
	 * @see #BaseAccount(String)
	 * @see #BaseAccount(String, String)
	 */
	@NonNull public final String getName() {
		return name;
	}

	/**
	 * Sets a new password for this account.
	 *
	 * @param password The desired new password. May be {@code null} to clear the current one.
	 *
	 * @see #BaseAccount(String, String)
	 * @see #getPassword()
	 */
	public final void setPassword(@Nullable final String password) {
		this.password = password;
	}

	/**
	 * Returns the password specified for this account.
	 *
	 * @return This account's password. May be {@code null}.
	 *
	 * @see #setPassword(String)
	 */
	@Nullable public final String getPassword() {
		return password;
	}

	/**
	 * Returns the types of authentication tokens specific for this account.
	 *
	 * @return For this account specific authentication token types.
	 */
	@Nullable protected String[] getAuthTokenTypes() {
		return AUTH_TOKEN_TYPES;
	}

	/**
	 * Sets an <i>OAuth</i> authentication token for this account.
	 *
	 * @param token The desired <b>OAuth</b> token. May be {@code null} to clear the previous one.
	 */
	public void setOAuthToken(@Nullable final String token) {
		if (token == null) removeAuthToken(TOKEN_TYPE_O_AUTH);
		else putAuthToken(TOKEN_TYPE_O_AUTH, token);
	}

	/**
	 * Returns the <i>OAuth</i> authentication token specified for this account.
	 *
	 * @return This account's <i>OAuth</i> token. May be {@code null} if not available.
	 */
	@Nullable public String getOAuthToken() {
		return getAuthToken(TOKEN_TYPE_O_AUTH);
	}

	/**
	 * Puts the given <var>token</var> into authentication tokens of this account under the
	 * given <var>tokenType</var> key.
	 *
	 * @param tokenType The type of the given token.
	 * @param token     The desired authentication token. May be {@code null} to clear the previous one.
	 *
	 * @see #getAuthToken(String)
	 * @see #getAuthTokens()
	 * @see #hasAuthToken(String)
	 * @see #removeAuthToken(String)
	 */
	protected void putAuthToken(@NonNull final String tokenType, @NonNull final String token) {
		if (authTokens == null) this.authTokens = new HashMap<>(1);
		this.authTokens.put(tokenType, token);
	}

	/**
	 * Checks whether there is authentication token specified for this user account for the specified
	 * <var>tokenType</var>.
	 *
	 * @param tokenType Type of the desired token to check if it is specified for this account.
	 * @return {@code True} if {@link #putAuthToken(String, String)} has been called with the
	 * <var>tokenType</var>, {@code false} otherwise.
	 *
	 * @see #putAuthToken(String, String)
	 */
	protected boolean hasAuthToken(@Nullable final String tokenType) {
		return authTokens != null && authTokens.containsKey(tokenType);
	}

	/**
	 * Removes a token with the given <var>tokenType</var> from the authentication tokens of this account.
	 *
	 * @param tokenType The type of the token to remove.
	 *
	 * @see #putAuthToken(String, String)
	 * @see #hasAuthToken(String)
	 */
	protected void removeAuthToken(@NonNull final String tokenType) {
		if (authTokens != null) this.authTokens.remove(tokenType);
	}

	/**
	 * Returns the authentication token specified for this account for the given <var>tokenType</var>.
	 *
	 * @param tokenType Type of the desired token to obtain.
	 * @return Authentication token for the given token type or {@code null} if there is no token
	 * specified for that type.
	 *
	 * @see #putAuthToken(String, String)
	 * @see #hasAuthToken(String)
	 * @see #removeAuthToken(String)
	 */
	@Nullable protected String getAuthToken(@Nullable final String tokenType) {
		return authTokens == null ? null : authTokens.get(tokenType);
	}

	/**
	 * Returns the map with authentication tokens specified for this account mapped to their types.
	 *
	 * @return This account's authentication tokens or {@code null} if there are no tokens specified yet.
	 *
	 * @see #putAuthToken(String, String)
	 * @see #getAuthToken(String)
	 */
	@Nullable protected Map<String, String> getAuthTokens() {
		return authTokens;
	}

	/**
	 * Puts the given <var>data</var> into data bundle of this account for the given <var>key</var>.
	 *
	 * @param key  The key to which to map the given data value.
	 * @param data The desired data to put into bundle.
	 *
	 * @see #getData(String)
	 * @see #getDataBundle()
	 */
	protected void putData(@NonNull final String key, @Nullable final String data) {
		if (dataBundle == null) this.dataBundle = new Bundle();
		this.dataBundle.putString(key, data);
	}

	/**
	 * Checks whether this account has data with the given <var>key</var> associated.
	 *
	 * @param key The key of the desired data to check.
	 * @return {@code True} if this account has data with the key, {@code false} otherwise.
	 *
	 * @see #putData(String, String)
	 */
	protected boolean hasData(@NonNull final String key) {
		return dataBundle != null && dataBundle.containsKey(key);
	}

	/**
	 * Returns the data value contained within data bundle of this account mapped to the given <var>key</var>.
	 *
	 * @param key The key for which to obtain the desired data.
	 * @return Data value for the specified key or {@code null} if there is no such value mapped for
	 * that key.
	 *
	 * @see #putData(String, String)
	 * @see #hasData(String)
	 * @see #getDataBundle()
	 */
	@Nullable protected String getData(@NonNull final String key) {
		return dataBundle == null ? null : dataBundle.getString(key);
	}

	/**
	 * Removes the data specified for this account for the given <var>key</var>.
	 *
	 * @param key The key for which to remove the desired data.
	 *
	 * @see #putData(String, String)
	 * @see #hasData(String)
	 */
	protected void removeData(@NonNull final String key) {
		if (dataBundle != null) dataBundle.remove(key);
	}

	/**
	 * Sets a data bundle for this account.
	 *
	 * @param dataBundle The desired bundle with data. May be {@code null} to clear the current bundle.
	 *
	 * @see #putData(String, String)
	 * @see #getData(String)
	 */
	protected void setDataBundle(@Nullable final Bundle dataBundle) {
		this.dataBundle = dataBundle;
	}

	/**
	 * Returns the data bundle of this account
	 *
	 * @return Bundle with data either specified via {@link #setDataBundle(Bundle)} or separately
	 * via {@link #putData(String, String)} or {@code null} if there is no data bundle available.
	 *
	 * @see #setDataBundle(Bundle)
	 * @see #getData(String)
	 */
	@Nullable protected Bundle getDataBundle() {
		return dataBundle;
	}

	/**
	 * <b>Note</b> that hashing and equals functions assume that name is unique for each account.
	 */
	@Override public int hashCode() {
		return name.hashCode();
	}

	/**
	 * <b>Note</b> that hashing and equals functions assume that name is unique for each account.
	 */
	@Override public boolean equals(@Nullable final Object other) {
		if (other == this) return true;
		if (!(other instanceof BaseAccount)) return false;
		final BaseAccount account = (BaseAccount) other;
		return this.name.equals(account.name);
	}

	/**
	 */
	@SuppressWarnings("StringBufferReplaceableByString")
	@Override @NonNull public String toString() {
		final StringBuilder builder = new StringBuilder(32);
		builder.append(getClass().getSimpleName());
		builder.append("(name: ");
		builder.append(name);
		return builder.append(")").toString();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}