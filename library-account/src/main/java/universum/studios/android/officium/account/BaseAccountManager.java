/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.annotation.VisibleForTesting;
import universum.studios.android.crypto.Crypto;
import universum.studios.android.crypto.Encrypto;
import universum.studios.android.crypto.util.CryptographyUtils;

/**
 * Base manager that wraps {@link AccountManager} in order to simplify management application accounts.
 * Each instance of BaseAccountManager can manage accounts only of a single type that is specified
 * during initialization via {@link #BaseAccountManager(Context, String)} constructor.
 * BaseAccountManager may be used for addition, update and also removal operations of accounts via
 * {@link #addAccount(BaseAccount)}, {@link #updateAccount(BaseAccount)} and {@link #removeAccount(BaseAccount)}
 * <p>
 * This manager also provides API methods to store and peek authentication tokens for a specific
 * account via {@link #setAccountAuthToken(String, String, String)} and {@link #peekAccountAuthToken(String, String)}.
 * Data for a specific account can be persisted either as single values via {@link #setAccountData(String, String, String)}
 * or as data {@link Bundle} via {@link #setAccountDataBundle(String, Bundle)} which is basically
 * bulk operation for the single value persistence approach. Persisted account data can be then
 * obtained via {@link #getAccountData(String, String)} or via {@link #getAccountDataBundle(String, String[])}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 *
 * @param <A> Type of the account that will be managed by the BaseAccountManager subclass.
 */
public abstract class BaseAccountManager<A extends BaseAccount> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "BaseAccountManager";

	/**
	 * Value for Android permission that allows to <b>GET</b> accounts.
	 */
	public static final String PERMISSION_GET_ACCOUNTS = Manifest.permission.GET_ACCOUNTS;

	/**
	 * Value for Android permission that allows to <b>MANAGE</b> accounts.
	 */
	public static final String PERMISSION_MANAGE_ACCOUNTS = "android.permission.MANAGE_ACCOUNTS";

	/**
	 * Value for Android permission that allows to <b>AUTHENTICATE</b> accounts.
	 */
	public static final String PERMISSION_AUTHENTICATE_ACCOUNTS = "android.permission.AUTHENTICATE_ACCOUNTS";

	/**
	 * Error indicating that an operation was requested for account that does not exist.
	 */
	public static final String ERROR_ACCOUNT_DOES_NOT_EXIST = "universum.studios.android.officium.account.AccountManager.ERROR.AccountDoesNotExist";

	/**
	 * Error indicating that an operation finished with system failure, that is, {@link AccountManager}
	 * returned some error.
	 */
	public static final String ERROR_SYSTEM_OPERATION_FAILURE = "universum.studios.android.officium.account.AccountManager.ERROR.SystemOperationFailure";

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Observer that may be used to observe account <b>additions, updates and removals</b> performed
	 * via {@link #addAccount(BaseAccount)}, {@link #updateAccount(BaseAccount)} or {@link #removeAccount(BaseAccount)}.
	 *
	 * @author Martin Albedinsky
	 * @since 2.0
	 *
	 * @param <A> Type of the account managed by the {@link BaseAccountManager} to which will be this
	 *            observer registered.
	 *
	 * @see #registerObserver(AccountObserver)
	 * @see #unregisterObserver(AccountObserver)
	 */
	public interface AccountObserver<A extends BaseAccount> {

		/**
		 * Invoked whenever a new {@link Account} has been added into the system for the specified
		 * <var>account</var> as result of calling {@link #addAccount(A)}.
		 *
		 * @param account The account that was added into the system.
		 */
		void onAccountAdded(@NonNull A account);

		/**
		 * Invoked whenever an existing {@link Account} along with its data has been updated in the
		 * system for the specified <var>account</var> as result of calling {@link #updateAccount(A)}.
		 *
		 * @param account The account that was updated in the system.
		 */
		void onAccountUpdated(@NonNull A account);

		/**
		 * Invoked whenever an existing {@link Account} has been removed from the system for the
		 * specified <var>account</var> as result of calling {@link #removeAccount(A)}.
		 *
		 * @param account The account that was removed from the system.
		 */
		void onAccountRemoved(@NonNull A account);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * System account manager used to add/update/remove accounts of the type specified for this manager.
	 */
	private final AccountManager systemManager;

	/**
	 * Type of accounts that can be managed by this manager.
	 */
	private final String accountType;

	/**
	 * Encrypto implementation that is used to encrypt keys of account data managed by this manager.
	 *
	 * @see #encryptKey(String)
	 */
	private Encrypto keyEncrypto;

	/**
	 * Crypto implementation that is used to encrypt and decrypt account data managed by this manager.
	 *
	 * @see #encrypt(String)
	 * @see #decrypt(String)
	 */
	private Crypto crypto;

	/**
	 * Handler that is used to dispatch callbacks on the main thread.
	 */
	private final Handler mainHandler;

	/**
	 * List with observers that may be registered via {@link #registerObserver(AccountObserver)}.
	 */
	private final List<AccountObserver<A>> observers = new ArrayList<>(1);

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of BaseAccountManager with the specified <var>accountType</var>.
	 *
	 * @param context     The context used to obtain system {@link AccountManager} to be used by the
	 *                    new manager.
	 * @param accountType The desired type of accounts that can be managed by the new manager.
	 */
	protected BaseAccountManager(@NonNull final Context context, @NonNull final String accountType) {
		this(AccountManager.get(context), accountType);
	}

	/**
	 * Creates a new instance of BaseAccountManager with the specified <var>systemManager</var> and
	 * <var>accountType</var>.
	 *
	 * @param systemManager The system {@link AccountManager} to be used by the new manager.
	 * @param accountType   The desired type of accounts that can be managed by the new manager.
	 */
	@VisibleForTesting BaseAccountManager(final AccountManager systemManager, final String accountType) {
		this.systemManager = systemManager;
		this.accountType = accountType;
		this.mainHandler = new Handler(Looper.getMainLooper());
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the type of accounts that this manager can manage (<i>add, update, remove</i>).
	 *
	 * @return The associated account type.
	 */
	@NonNull public final String getAccountType() {
		return accountType;
	}

	/**
	 * Returns the system manager that is used by this manager to create/update/remove accounts in
	 * and from the system.
	 *
	 * @return System manager ready to be used.
	 */
	@NonNull protected final AccountManager getSystemManager() {
		return systemManager;
	}

	/**
	 * Sets an implementation of {@link Encrypto} that should be used by this manager to perform
	 * encryption of account data keys.
	 *
	 * @param encrypto The desired encrypto implementation. May be {@code null} to not perform keys
	 *                 encryption.
	 *
	 * @see #setCrypto(Crypto)
	 */
	public final void setKeyEncrypto(@Nullable final Encrypto encrypto) {
		this.keyEncrypto = encrypto;
	}

	/**
	 * Sets an implementation of {@link Crypto} that should be used by this manager to perform
	 * encryption/decryption of account data (authentication tokens, user data).
	 *
	 * @param crypto The desired crypto implementation. May be {@code null} to not perform data
	 *               encryption/decryption.
	 *
	 * @see #setKeyEncrypto(Encrypto)
	 */
	public final void setCrypto(@Nullable final Crypto crypto) {
		this.crypto = crypto;
	}

	/**
	 * Encrypts keys and data contained within the specified <var>bundle</var>.
	 *
	 * @param bundle The desired bundle to be encrypted.
	 * @return Bundle with encrypted keys and data or the same bundle if there is no cryptographic
	 * tool specified.
	 *
	 * @see #encryptKey(String)
	 * @see #encrypt(String)
	 */
	@VisibleForTesting Bundle encryptBundle(@Nullable final Bundle bundle) {
		if (bundle == null || bundle.isEmpty()) {
			return bundle;
		}
		final Bundle encryptedBundle = new Bundle();
		final Set<String> keys = bundle.keySet();
		for (final String key : keys) {
			encryptedBundle.putString(encryptKey(key), encrypt(bundle.getString(key)));
		}
		return encryptedBundle;
	}

	/**
	 * Encrypts the specified <var>key</var> using {@link #keyEncrypto}, if presented.
	 *
	 * @param key The desired key to be encrypted.
	 * @return Encrypted key or the same key if there is no cryptographic tool specified.
	 *
	 * @see #encrypt(String)
	 * @see #decrypt(String)
	 */
	@Nullable protected final String encryptKey(@NonNull final String key) {
		return keyEncrypto == null ? key : CryptographyUtils.encrypt(key, keyEncrypto);
	}

	/**
	 * Encrypts the specified <var>value</var> using {@link #crypto}, if presented.
	 *
	 * @param value The desired data value to be encrypted. May be {@code null}.
	 * @return Encrypted data value or the same value if there is no cryptographic tool specified.
	 *
	 * @see #decrypt(String)
	 */
	@Nullable protected final String encrypt(@Nullable final String value) {
		return crypto == null || value == null ? value : CryptographyUtils.encrypt(value, crypto);
	}

	/**
	 * Decrypts the specified <var>value</var> using {@link #crypto}, if presented.
	 *
	 * @param value The desired data value to be decrypted. May be {@code null}.
	 * @return Decrypted data value or the same value if there is no cryptographic tool specified.
	 *
	 * @see #encrypt(String)
	 */
	@Nullable protected final String decrypt(@Nullable final String value) {
		return crypto == null || value == null ? value : CryptographyUtils.decrypt(value, crypto);
	}

	/**
	 * Registers the specified <var>observer</var> to be notified whenever a new account is added
	 * into the system, an existing one updated in the system or removed from the system.
	 *
	 * @param observer The desired observer to register if it is not yet registered.
	 *
	 * @see #unregisterObserver(AccountObserver)
	 */
	public final void registerObserver(@NonNull final AccountObserver<A> observer) {
		if (!observers.contains(observer)) this.observers.add(observer);
	}

	/**
	 * Notifies all registered observers that the given <var>account</var> was added into the system.
	 *
	 * @param account The account that was added.
	 */
	final void notifyAccountAdded(final A account) {
		if (!observers.isEmpty()) {
			this.mainHandler.post(new Runnable() {

				/**
				 */
				@Override public void run() {
					for (final AccountObserver<A> observer : observers) {
						observer.onAccountAdded(account);
					}
				}
			});
		}
	}

	/**
	 * Notifies all registered observers that the given <var>account</var> was updated in the system.
	 *
	 * @param account The account that was updated.
	 */
	final void notifyAccountUpdated(final A account) {
		if (!observers.isEmpty()) {
			this.mainHandler.post(new Runnable() {

				/**
				 */
				@Override public void run() {
					for (final AccountObserver<A> observer : observers) {
						observer.onAccountUpdated(account);
					}
				}
			});
		}
	}

	/**
	 * Notifies all registered observers that the given <var>account</var> was removed from the system.
	 *
	 * @param account The account that was removed.
	 */
	final void notifyAccountRemoved(final A account) {
		if (!observers.isEmpty()) {
			this.mainHandler.post(new Runnable() {

				/**
				 */
				@Override public void run() {
					for (final AccountObserver<A> observer : observers) {
						observer.onAccountRemoved(account);
					}
				}
			});
		}
	}

	/**
	 * Unregisters the specified <var>observer</var> that was previously registered so it will be no
	 * longer notified about account operations.
	 *
	 * @param observer The desired observer to be unregistered.
	 *
	 * @see #registerObserver(AccountObserver)
	 */
	public final void unregisterObserver(@NonNull final AccountObserver<A> observer) {
		this.observers.remove(observer);
	}

	/**
	 * Adds a new system account for the given <var>account</var> into the system with all its
	 * associated data (password, user data, authentication tokens).
	 *
	 * @param account The desired account that should be added along with all its associated data.
	 * @return Either successful or failed result. If such account already exists this operation will
	 * finishes with successful result.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull public AccountResult addAccount(@NonNull final A account) {
		final AccountResult result = onAddAccount(account);
		if (result.isSuccess()) {
			notifyAccountAdded(account);
		}
		return result;
	}

	/**
	 * Invoked whenever {@link #addAccount(BaseAccount)} is called on this account instance.
	 * <p>
	 * Default implementation adds a new system account via {@link AccountManager#addAccountExplicitly(Account, String, Bundle)}
	 * and also sets account's associated authentication tokens.
	 * <p>
	 * If the account already exists successful result is returned.
	 *
	 * @param account The account to be added into the system along with all its associated data.
	 * @return Either successful or failed result depending on the result of add operation.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull protected AccountResult onAddAccount(@NonNull final A account) {
		if (findSystemAccount(account.getName()) != null) {
			return AccountResults.createSuccess();
		}
		final Account systemAccount = new Account(account.getName(), getAccountType());
		if (systemManager.addAccountExplicitly(systemAccount, encrypt(account.getPassword()), encryptBundle(account.getDataBundle()))) {
			final String[] authTokenTypes = account.getAuthTokenTypes();
			final Map<String, String> authTokens = account.getAuthTokens();
			if (authTokenTypes != null && authTokenTypes.length > 0 && authTokens != null && !authTokens.isEmpty()) {
				for (final String authTokenType : authTokenTypes) {
					this.systemManager.setAuthToken(systemAccount, encryptKey(authTokenType), encrypt(authTokens.get(authTokenType)));
				}
			}
			return AccountResults.createSuccess();
		}
		return AccountResults.createFailure(AccountErrors.create(ERROR_SYSTEM_OPERATION_FAILURE));
	}

	/**
	 * Updates data of the given <var>account</var> in the system, particularly password,
	 * authentication tokens and user data.
	 *
	 * @param account The desired account that should be updated along with all its associated data.
	 * @return Either successful or failed result. If such account does not exist this operation fails
	 * with {@link #ERROR_ACCOUNT_DOES_NOT_EXIST} error.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull public AccountResult updateAccount(@NonNull final A account) {
		final AccountResult result = onUpdateAccount(account);
		if (result.isSuccess()) {
			notifyAccountUpdated(account);
		}
		return result;
	}

	/**
	 * Invoked whenever {@link #updateAccount(BaseAccount)} is called on this account instance.
	 * <p>
	 * Default implementation updates account's password and also updates all authentication tokens
	 * and user data for that account.
	 * <p>
	 * If the account does not exist failed result with {@link #ERROR_ACCOUNT_DOES_NOT_EXIST} is returned.
	 *
	 * @param account The account to be updated in the system along with all its associated data.
	 * @return Either successful or failed result depending on the result of update operations.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull protected AccountResult onUpdateAccount(@NonNull final A account) {
		final Account systemAccount = findSystemAccount(account.getName());
		if (systemAccount == null) {
			return AccountResults.createFailure(AccountErrors.create(ERROR_ACCOUNT_DOES_NOT_EXIST));
		}
		this.systemManager.setPassword(systemAccount, encrypt(account.getPassword()));
		final String[] authTokenTypes = account.getAuthTokenTypes();
		final Map<String, String> authTokens = account.getAuthTokens();
		if (authTokenTypes != null && authTokenTypes.length > 0 && authTokens != null && !authTokens.isEmpty()) {
			for (final String authTokenType : authTokenTypes) {
				this.systemManager.setAuthToken(systemAccount, encryptKey(authTokenType), encrypt(authTokens.get(authTokenType)));
			}
		}
		final Bundle dataBundle = account.getDataBundle();
		if (dataBundle != null) {
			for (final String key : dataBundle.keySet()) {
				this.systemManager.setUserData(systemAccount, encryptKey(key), encrypt(dataBundle.getString(key)));
			}
		}
		return AccountResults.createSuccess();
	}

	/**
	 * Removes system account associated with the given <var>account</var> from the system with all
	 * its associated data.
	 *
	 * @param account The desired account that should be removed along with all its associated data.
	 * @return Either successful or failed result. If such account does not exist this operation fails
	 * with {@link #ERROR_ACCOUNT_DOES_NOT_EXIST} error.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_MANAGE_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull public AccountResult removeAccount(@NonNull final A account) {
		final AccountResult result = onRemoveAccount(account);
		if (result.isSuccess()) {
			notifyAccountRemoved(account);
		}
		return result;
	}

	/**
	 * Invoked whenever {@link #removeAccount(BaseAccount)} is called on this account instance.
	 * <p>
	 * Default implementation removes the account via {@link AccountManager#removeAccount(Account, Activity, AccountManagerCallback, Handler)},
	 * sets account's password to {@code null} and also invalidates all authentication tokens for
	 * that account.
	 * <p>
	 * If the account does not exist failed result with {@link #ERROR_ACCOUNT_DOES_NOT_EXIST} is returned.
	 *
	 * @param account The account to be removed from the system along with all its associated data.
	 * @return Either successful or failed result depending on the result of remove operation.
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_MANAGE_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@NonNull protected AccountResult onRemoveAccount(@NonNull final A account) {
		final Account systemAccount = findSystemAccount(account.getName());
		if (systemAccount == null) {
			return AccountResults.createFailure(AccountErrors.create(ERROR_ACCOUNT_DOES_NOT_EXIST));
		}
		Bundle operationResult = Bundle.EMPTY;
		AccountError error = AccountError.NONE;
		try {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
				operationResult = systemManager.removeAccount(systemAccount, null, null, null).getResult();
			} else {
				final boolean success = systemManager.removeAccount(systemAccount, null, null).getResult();
				operationResult = new Bundle();
				operationResult.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, success);
			}
		} catch (OperationCanceledException | IOException | AuthenticatorException e) {
			error = AccountErrors.create(ERROR_SYSTEM_OPERATION_FAILURE, e);
		}
		if (error != AccountError.NONE) {
			return AccountResults.createFailure(error);
		}
		this.systemManager.setPassword(systemAccount, null);
		final String[] authTokenTypes = account.getAuthTokenTypes();
		if (authTokenTypes != null && authTokenTypes.length > 0) {
			for (final String authTokenType : authTokenTypes) {
				this.systemManager.invalidateAuthToken(systemAccount.type, systemManager.peekAuthToken(systemAccount, encryptKey(authTokenType)));
			}
		}
		// todo: should we also remove data values ???
		return AccountResults.createSuccess(operationResult);
	}

	/**
	 * Checks whether account with the specified <var>accountName</var> is authenticated or not.
	 * <p>
	 * By default an account is considered authenticated if there is persisted authentication token
	 * for that account with the specified <var>tokenType</var> in the system.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the account for which to check its authentication state.
	 * @param tokenType Type of the authentication token used to peek that token in order to resolve
	 *                  whether the account is authenticated or not.
	 * @return {@code True} if valid authentication token is persisted for the account with the
	 * specified token type, {@code false} otherwise.
	 *
	 * @see #setAccountAuthToken(String, String, String)
	 * @see #peekAccountAuthToken(String, String)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	public boolean isAccountAuthenticated(@NonNull final String accountName, @NonNull final String tokenType) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return false;
		}
		return !TextUtils.isEmpty(systemManager.peekAuthToken(systemAccount, encryptKey(tokenType)));
	}

	/**
	 * Persists the given authentication <var>token</var> for an account with the specified
	 * <var>accountName</var>.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the account for which to persist its authentication token.
	 * @param tokenType Type of the token to be used as a key by which the desired token may be later peeked.
	 * @param token The desired token to be persisted.
	 *
	 * @see #peekAccountAuthToken(String, String)
	 * @see #isAccountAuthenticated(String, String)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	public void setAccountAuthToken(@NonNull final String accountName, @NonNull final String tokenType, @NonNull final String token) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount != null) {
			this.systemManager.setAuthToken(systemAccount, encryptKey(tokenType), encrypt(token));
		}
	}

	/**
	 * Returns the authentication token for an account with the given <var>accountName</var> persisted
	 * for the specified <var>tokenType</var>.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the account for which to peek its authentication token.
	 * @param tokenType Type of the desired authentication token to peek.
	 * @return Authentication token or {@code null} if there has not been persisted token for the
	 * account or the specified type yet or the token has been already invalidated.
	 *
	 * @see #setAccountAuthToken(String, String, String)
	 * @see #invalidateAccountAuthToken(String, String)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@Nullable public String peekAccountAuthToken(@NonNull final String accountName, @NonNull final String tokenType) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return null;
		}
		return decrypt(systemManager.peekAuthToken(systemAccount, encryptKey(tokenType)));
	}

	/**
	 * Invalidates an authentication token with the specified <var>tokenType</var> persisted for an
	 * account with the specified <var>accountName</var>.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b>,
	 * <b>{@link #PERMISSION_MANAGE_ACCOUNTS}</b> and <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b>
	 * permissions.
	 *
	 * @param accountName Name of the account for which to invalidate its authentication token.
	 * @param tokenType Type of the desired authentication token to invalidate.
	 * @return {@code True} if the desired token has been invalidated, {@code false} if there is no
	 * such token persisted for the account or the token type.
	 *
	 * @see #peekAccountAuthToken(String, String)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_MANAGE_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	public boolean invalidateAccountAuthToken(@NonNull final String accountName, @NonNull final String tokenType) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return false;
		}
		final String token = systemManager.peekAuthToken(systemAccount, encryptKey(tokenType));
		if (token == null) {
			return false;
		}
		this.systemManager.invalidateAuthToken(systemAccount.type, token);
		return true;
	}

	/**
	 * Persists a single data <var>value</var> for account with the specified <var>accountName</var>.
	 * <p>
	 * <b>Note that if either {@link Encrypto} for keys or {@link Crypto} for data has been specified
	 * for this manager via {@link #setKeyEncrypto(Encrypto)} or via {@link #setCrypto(Crypto)}
	 * the given key and data value will be encrypted, respectively, before persisted.</b>
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the desired account for which to update its single data.
	 * @param key         Key of the single data value to be persisted.
	 * @param value       The desired single data value to be persisted. May be {@code null} in order
	 *                    to remove previously persisted value.
	 *
	 * @see #getAccountData(String, String)
	 * @see #setAccountDataBundle(String, Bundle)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	protected void setAccountData(@NonNull final String accountName, @NonNull final String key, @Nullable final String value) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return;
		}
		this.systemManager.setUserData(systemAccount, encryptKey(key), encrypt(value));
	}

	/**
	 * Obtains single data <var>value</var> persisted for account with the specified <var>accountName</var>.
	 * <p>
	 * <b>Note that if {@link Crypto} for data has been specified via {@link #setCrypto(Crypto)}
	 * the result data value will be decrypted before returned.</b>
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the desired account for which to obtain its single data.
	 * @param key         Key of the desired single data value to be obtained.
	 * @return Single data value (decrypted if necessary) for the requested key or {@code null}
	 * if there is no such data persisted or account with the specified name does not exist.
	 *
	 * @see #setAccountData(String, String, String)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@Nullable protected String getAccountData(@NonNull final String accountName, @NonNull final String key) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return null;
		}
		return decrypt(systemManager.getUserData(systemAccount, encryptKey(key)));
	}

	/**
	 * Persists bundle of data <var>values</var> for account with the specified <var>accountName</var>.
	 * <p>
	 * <b>Note that if either {@link Encrypto} for keys or {@link Crypto} for data has been specified
	 * for this manager via {@link #setKeyEncrypto(Encrypto)} or via {@link #setCrypto(Crypto)}
	 * all keys and data values contained in the given bundle will be encrypted, respectively, before
	 * persisted.</b>
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the desired account for which to update its data.
	 * @param values      Bundle with desired data values to be persisted for the account.
	 *
	 * @see #setAccountData(String, String, String)
	 * @see #getAccountDataBundle(String, String...)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	protected void setAccountDataBundle(@NonNull final String accountName, @NonNull final Bundle values) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return;
		}
		for (final String key : values.keySet()) {
			this.systemManager.setUserData(systemAccount, encryptKey(key), encrypt(values.getString(key)));
		}
	}

	/**
	 * Obtains bundle of data <var>values</var> persisted for account with the specified <var>accountName</var>.
	 * <p>
	 * <b>Note that if {@link Crypto} for data has been specified via {@link #setCrypto(Crypto)}
	 * all data values contained in the result bundle will be decrypted before returned.</b>
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> and
	 * <b>{@link #PERMISSION_AUTHENTICATE_ACCOUNTS}</b> permissions.
	 *
	 * @param accountName Name of the desired account for which to obtain its data bundle.
	 * @param keys        Set of keys of the desired data values to be obtained.
	 * @return Bundle containing all data values (decrypted if necessary) for the requested keys or
	 * {@code null} if account with the specified name does not exist.
	 *
	 * @see #setAccountDataBundle(String, Bundle)
	 */
	@RequiresPermission(allOf = {PERMISSION_GET_ACCOUNTS, PERMISSION_AUTHENTICATE_ACCOUNTS})
	@Nullable protected Bundle getAccountDataBundle(@NonNull final String accountName, @NonNull final String[] keys) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return null;
		}
		final Bundle dataBundle = new Bundle();
		for (final String key : keys) {
			dataBundle.putString(key, decrypt(systemManager.getUserData(systemAccount, encryptKey(key))));
		}
		return dataBundle;
	}

	/**
	 * Searches current system accounts by the account type specified for this manager using the
	 * given account <var>accountName</var> and if such system account is found creates a new
	 * instance of account specific for this manager with all authentication tokens and appropriate
	 * data attached to it.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> permission.
	 *
	 * @param accountName Name of the desired account to find.
	 * @return Requested account for which exists system account with the specified name or {@code null}
	 * if no such account exists in the system.
	 *
	 * @see #addAccount(BaseAccount)
	 */
	@RequiresPermission(PERMISSION_GET_ACCOUNTS)
	@Nullable public A findAccount(@NonNull final String accountName) {
		final Account systemAccount = findSystemAccount(accountName);
		if (systemAccount == null) {
			return null;
		}
		return onCreateAccountInstance(systemManager, systemAccount);
	}

	/**
	 * Invoked whenever this manager needs a new instance of account, for example this methods is
	 * called whenever {@link #findAccount(String)} actually finds the given <var>systemAccount</var>
	 * for the requested name.
	 *
	 * @param systemManager System manager that may be used to obtain specific data associated with
	 *                      the requested account that are persisted in the system.
	 * @param systemAccount System account associated with the requested account.
	 * @return New account instance.
	 */
	@NonNull protected abstract A onCreateAccountInstance(@NonNull final AccountManager systemManager, @NonNull final Account systemAccount);

	/**
	 * Called to find the Android system {@link Account} with the given <var>accountName</var>.
	 * <p>
	 * Default implementation obtains all current accounts by the account type specified for this
	 * manager and searches for one that has the given name specified.
	 * <p>
	 * This method requires the caller to hold <b>{@link #PERMISSION_GET_ACCOUNTS}</b> permission.
	 *
	 * @param accountName Name of the desired system account to find.
	 * @return System account that has been previously added to the system with the specified name
	 * or {@code null} if there is no such account created.
	 *
	 * @see Account#name
	 * @see AccountManager#getAccountsByType(String)
	 */
	@RequiresPermission(PERMISSION_GET_ACCOUNTS)
	@Nullable protected Account findSystemAccount(@NonNull final String accountName) {
		final Account[] accounts = systemManager.getAccountsByType(accountType);
		if (accounts.length > 0) {
			for (final Account account : accounts) {
				if (account.name.equals(accountName)) return account;
			}
		}
		return null;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}