/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import androidx.annotation.NonNull;

/**
 * A convenience factory class which may be used to create {@link AccountError} instances.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class AccountErrors {

    /**
     */
    private AccountErrors() {
        // Not allowed to be instantiated publicly.
        throw new UnsupportedOperationException();
    }

    /**
     * Same as {@link #create(String, Throwable)} with <var>none</var> cause.
     */
    @NonNull public static AccountError create(@NonNull final String code) {
        return create(code, AccountError.NONE.cause());
    }

    /**
     * Creates a new instance of AccountError with the specified <var>code</var> and <var>cause</var>.
     *
     * @param code The desired code for the new error.
     * @param cause The cause due to which the new error occurred.
     * @return Error ready to be propagated further.
     */
    @NonNull public static AccountError create(@NonNull final String code, @NonNull final Throwable cause) {
        return new ErrorImpl(code, cause);
    }

    /**
     * Implementation of {@link AccountError} which provides {@link #code()} and {@link #cause()} accessors.
     */
    private static class ErrorImpl implements AccountError {

        /**
         * Code identifying this error.
         */
        private final String code;

        /**
         * Cause describing why this error occurred.
         */
        private final Throwable cause;

        /**
         * Creates a new instance of ErrorImpl with the given <var>code</var> and <var>cause</var>.
         *
         * @param code The desired code for the new error.
         * @param cause The cause due to which the new error occurred.
         */
        ErrorImpl(final String code, final Throwable cause) {
            this.code = code;
            this.cause = cause;
        }

        /**
         */
        @Override @NonNull public String code() {
            return code;
        }

        /**
         */
        @Override @NonNull public Throwable cause() {
            return cause;
        }

        /**
         */
        @SuppressWarnings("StringBufferReplaceableByString")
        @Override @NonNull public String toString() {
            final StringBuilder builder = new StringBuilder(32);
            builder.append(AccountError.class.getSimpleName());
            builder.append("(code: ");
            builder.append(code);
            builder.append(", cause: ");
            builder.append(cause);
            return builder.append(")").toString();
        }
    }
}