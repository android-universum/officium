/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import androidx.annotation.NonNull;

/**
 * Account related error which may be identified by its {@link #code() code} and further described
 * via its {@link #cause() cause}.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public interface AccountError {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Instance which may be used to indicate a 'none' error.
	 */
	@NonNull AccountError NONE = new AccountError() {

		/**
		 * Empty cause of this error.
		 */
		private final Throwable cause = new Throwable() {

			/**
			 */
			@Override @NonNull public String toString() { return "NONE"; }
		};

		/**
		 */
		@Override @NonNull public String code() { return "NONE"; }

		/**
		 */
		@Override @NonNull public Throwable cause() { return cause; }

		/**
		 */
		@Override @NonNull public String toString() { return code(); }
	};

	/**
	 * Instance which may be used to indicate an 'unknown' error.
	 */
	@NonNull AccountError UNKNOWN = new AccountError() {

		/**
		 */
		@Override @NonNull public String code() { return "UNKNOWN"; }

		/**
		 */
		@Override @NonNull public Throwable cause() { return NONE.cause(); }

		/**
		 */
		@Override @NonNull public String toString() { return code(); }
	};

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Returns the code of this account error.
	 *
	 * @return This error's code.
	 *
	 * @see #cause()
	 */
	@NonNull String code();

	/**
	 * Returns the cause associated with this account error.
	 *
	 * @return This error's cause.
	 */
	@NonNull Throwable cause();
}