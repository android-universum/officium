/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.account;

import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * A convenience factory class which may be used to create {@link AccountResult} instances.
 *
 * @author Martin Albedinsky
 * @since 2.0
 */
public final class AccountResults {

    /**
     */
    private AccountResults() {
        // Not allowed to be instantiated publicly.
        throw new UnsupportedOperationException();
    }

	/**
	 * Returns an empty instance of {@link AccountResult} which has an empty value and
	 * the {@link AccountError#NONE} error associated.
	 *
	 * @return Empty result ready to be dispatched.
	 */
	@NonNull public static AccountResult empty() {
    	return ResultImpl.EMPTY;
    }

	/**
	 * Creates a new instance of successful AccountResult with {@link Bundle#EMPTY} value.
	 *
	 * The created result will have associated {@link AccountError#NONE} error.
	 *
	 * @return Successful result ready to be delivered.
	 *
	 * @see #createSuccess(Bundle)
	 */
	@NonNull public static AccountResult createSuccess() {
		return createSuccess(Bundle.EMPTY);
    }

	/**
	 * Creates a new instance of successful AccountResult with the specified <var>value</var>.
	 *
	 * The created result will have associated {@link AccountError#NONE} error.
	 *
	 * @param value The desired value for the new result.
	 * @return @return Successful result ready to be delivered.
	 * @see #createFailure(AccountError)
	 */
	@NonNull public static AccountResult createSuccess(@NonNull final Bundle value) {
		return new ResultImpl(value, AccountError.NONE);
    }

	/**
	 * Creates a new instance of failure AccountResult with the specified <var>error</var>.
	 *
	 * The created result will have associated {@link Bundle#EMPTY} value.
	 *
	 * @param error The desired error for the new result.
	 * @return Failure result ready to be delivered
	 *
	 * @see #createSuccess(Bundle)
	 */
	@NonNull public static AccountResult createFailure(@NonNull final AccountError error) {
		return new ResultImpl(null, error);
    }

	/**
	 * Implementation of {@link AccountResult} which provides {@link #isSuccess()} => {@code #getValue()}
	 * and {@link #isFailure()} => {@link #getError()} accessors.
     */
    private static class ResultImpl implements AccountResult {

		/**
		 * Empty instance with empty value and error.
		 */
		private static final AccountResult EMPTY = new ResultImpl(Bundle.EMPTY, AccountError.NONE);

		/**
		 * Result value which was successfully produced.
		 */
		private final Bundle value;

		/**
		 * Error due to which a result value could not be produced.
		 */
		private final AccountError error;

		/**
		 * Creates a new instance of ResultImpl with the specified <var>value</var> and <var>error</var>.
		 *
		 * @param value Result value which was successfully produced.
		 * @param error Error due to which a result value could not be produced.
		 */
		ResultImpl(final Bundle value, final AccountError error) {
			this.value = value;
			this.error = error;
		}

		/**
		 */
		@Override public boolean isSuccess() {
			return !isFailure();
		}

		/**
		 */
		@Override @NonNull public Bundle getValue() {
			return value;
		}

		/**
		 */
		@Override public boolean isFailure() {
			return error != AccountError.NONE;
		}

		/**
		 */
		@Override @NonNull public AccountError getError() {
			return error;
		}

		/**
		 */
		@SuppressWarnings("StringBufferReplaceableByString")
		@Override @NonNull public String toString() {
			final StringBuilder builder = new StringBuilder(64);
			builder.append(AccountResult.class.getSimpleName());
			builder.append("(isSuccess: ");
			builder.append(isSuccess());
			builder.append(", value: ");
			builder.append(value);
			builder.append(", isFailure: ");
			builder.append(isFailure());
			builder.append(", error: ");
			builder.append(error);
			return builder.append(")").toString();
		}
	}
}