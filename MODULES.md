Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/officium/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/officium/src/main/library-core)**
- **[Account](https://bitbucket.org/android-universum/officium/src/main/library-account)**
- **[@Event](https://bitbucket.org/android-universum/officium/src/main/library-event_group)**
- **[Event-Core](https://bitbucket.org/android-universum/officium/src/main/library-event-core)**
- **[Event-Common](https://bitbucket.org/android-universum/officium/src/main/library-event-common)**
- **[Service](https://bitbucket.org/android-universum/officium/src/main/library-service)**
- **[Sync](https://bitbucket.org/android-universum/officium/src/main/library-sync)**
