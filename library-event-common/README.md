Officium-Event-Common
===============

This module contains common implementations of `EventBus` interface.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aofficium/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aofficium/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:officium-event-common:${DESIRED_VERSION}@aar"

_depends on:_
[officium-event-core](https://bitbucket.org/android-universum/officium/src/main/library-event-core),
[`com.squareup:otto`](http://square.github.io/otto)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SimpleEventBus](https://bitbucket.org/android-universum/officium/src/main/library-event-common/src/main/java/universum/studios/android/officium/event/SimpleEventBus.java)
- [MainEventBus](https://bitbucket.org/android-universum/officium/src/main/library-event-common/src/main/java/universum/studios/android/officium/event/MainEventBus.java)