@Officium-Event
===============

This module groups the following modules into one **single group**:

- [Event-Core](https://bitbucket.org/android-universum/officium/src/main/library-event-core)
- [Event-Common](https://bitbucket.org/android-universum/officium/src/main/library-event-common)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aofficium/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aofficium/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:officium-event:${DESIRED_VERSION}@aar"

_depends on:_
[`com.squareup:otto`](http://square.github.io/otto)