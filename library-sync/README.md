Officium-Sync
===============

This module contains elements that may be used to build a **synchronization layer** for an
**Android** application backed by `AbstractThreadedSyncAdapter` in a simple and consistent way.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aofficium/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aofficium/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:officium-sync:${DESIRED_VERSION}@aar"

_depends on:_
[officium-core](https://bitbucket.org/android-universum/officium/src/main/library-core),
[`com.google.code.gson:gson`](https://github.com/google/gson)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseSyncManager](https://bitbucket.org/android-universum/officium/src/main/library-sync/src/main/java/universum/studios/android/officium/sync/BaseSyncManager.java)
- [BaseSyncAdapter](https://bitbucket.org/android-universum/officium/src/main/library-sync/src/main/java/universum/studios/android/officium/sync/BaseSyncAdapter.java)
- [SyncTask](https://bitbucket.org/android-universum/officium/src/main/library-sync/src/main/java/universum/studios/android/officium/sync/SyncTask.java)
- [SyncHandler](https://bitbucket.org/android-universum/officium/src/main/library-sync/src/main/java/universum/studios/android/officium/sync/SyncHandler.java)
- [SyncEvent](https://bitbucket.org/android-universum/officium/src/main/library-sync/src/main/java/universum/studios/android/officium/sync/SyncEvent.java)