Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 3.x ##

### [3.0.0](https://bitbucket.org/android-universum/officium/wiki/version/3.x) ###
> 27.04.2020

- **This release bumps minimum requirements to Android 5+**.
- Available for use with **[OkHttp 4.x](https://github.com/square/okhttp/blob/main/CHANGELOG.md)**.

## [Version 2.x](https://bitbucket.org/android-universum/officium/wiki/version/2.x) ##

## [Version 1.x](https://bitbucket.org/android-universum/officium/wiki/version/1.x) ##