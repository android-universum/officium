/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.service;

import org.junit.Test;

import okhttp3.HttpUrl;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Albedinsky
 */
public final class ServiceApiTest extends TestCase {

	@Test public void testInstantiation() {
		// Arrange:
		final ServiceManager mockManager = mock(ServiceManager.class);
		// Act:
		final ServiceApi<ServiceManager> api = new ServiceApi<>(mockManager);
		// Assert:
		assertThat(api.getManager(), is(mockManager));
		verifyNoMoreInteractions(mockManager);
	}

	@Test public void testServices() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(HttpUrl.get("https://google.com/"));
		final ServiceApi<ServiceManager> api = new ServiceApi<>(manager);
		// Act:
		final TestServices services = api.services(TestServices.class);
		// Assert:
		assertThat(services, is(notNullValue()));
		assertThat(services, is(instanceOf(TestServices.class)));
	}

	@Test public void testServicesConfiguration() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(HttpUrl.get("https://google.com/"));
		final ServiceApi<ServiceManager> api = new ServiceApi<>(manager);
		// Act:
		final ServiceManager.ServicesConfiguration<TestServices> configuration = api.servicesConfiguration(TestServices.class);
		// Assert:
		assertThat(configuration, is(notNullValue()));
		assertThat(configuration.services(), is(notNullValue()));
		assertThat(configuration.services(), is(instanceOf(TestServices.class)));
	}

	private interface TestServices {}
}