/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.officium.service;

import org.junit.Test;

import okhttp3.HttpUrl;
import universum.studios.android.test.TestCase;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertSame;

/**
 * @author Martin Albedinsky
 */
public final class ServiceManagerTest extends TestCase {

	private static final String BASE_URL = "https://www.android.com/";

	@SuppressWarnings("ConstantConditions")
	@Test public void testInstantiation() {
		// Arrange:
		final HttpUrl baseUrl = HttpUrl.parse(BASE_URL);
		// Act:
		final ServiceManager manager = new ServiceManager(baseUrl);
		// Assert:
		assertThat(manager.getBaseUrl(), is(baseUrl));
		assertThat(manager.getBaseUrl().toString(), is(BASE_URL));
	}

	@Test public void testInstantiationWithBaseUrlString() {
		// Act:
		final ServiceManager manager = new ServiceManager(BASE_URL);
		// Assert:
		assertThat(manager.getBaseUrl(), is(notNullValue()));
	}

	@Test public void testServices() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(BASE_URL);
		// Act + Assert:
		assertThat(manager.services(TestPrimaryServices.class), is(notNullValue()));
		assertSame(manager.services(TestPrimaryServices.class), manager.services(TestPrimaryServices.class));
		assertThat(manager.services(TestSecondaryServices.class), is(notNullValue()));
		assertSame(manager.services(TestSecondaryServices.class), manager.services(TestSecondaryServices.class));
		assertThat(manager.services(TestTertiaryServices.class), is(notNullValue()));
		assertSame(manager.services(TestTertiaryServices.class), manager.services(TestTertiaryServices.class));
	}

	@Test public void testServicesConfiguration() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(BASE_URL);
		// Act + Assert:
		assertThat(manager.servicesConfiguration(TestPrimaryServices.class), is(notNullValue()));
		assertSame(manager.servicesConfiguration(TestPrimaryServices.class), manager.servicesConfiguration(TestPrimaryServices.class));
		assertThat(manager.servicesConfiguration(TestSecondaryServices.class), is(notNullValue()));
		assertSame(manager.servicesConfiguration(TestSecondaryServices.class), manager.servicesConfiguration(TestSecondaryServices.class));
		assertThat(manager.servicesConfiguration(TestTertiaryServices.class), is(notNullValue()));
		assertSame(manager.servicesConfiguration(TestTertiaryServices.class), manager.servicesConfiguration(TestTertiaryServices.class));
	}

	@Test public void testServicesConfigurations() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(BASE_URL);
		manager.servicesConfiguration(TestPrimaryServices.class);
		manager.servicesConfiguration(TestSecondaryServices.class);
		manager.servicesConfiguration(TestTertiaryServices.class);
		// Act:
		final ServiceManager.ServicesConfiguration<?>[] configurations = manager.getServicesConfigurations();
		// Assert:
		assertThat(configurations, is(notNullValue()));
		assertThat(configurations.length, is(3));
		for (ServiceManager.ServicesConfiguration<?> configuration : configurations) {
			assertThat(configuration.services(), is(notNullValue()));
			assertThat(configuration.services(), is(anyOf(instanceOf(TestPrimaryServices.class), instanceOf(TestSecondaryServices.class), instanceOf(TestTertiaryServices.class))));
		}
	}

	@Test public void testServicesConfigurationsWhenEmpty() {
		// Arrange:
		final ServiceManager manager = new ServiceManager(BASE_URL);
		// Act:
		final ServiceManager.ServicesConfiguration<?>[] configurations = manager.getServicesConfigurations();
		// Assert:
		assertThat(configurations, is(notNullValue()));
		assertThat(configurations.length, is(0));
	}

	interface TestPrimaryServices {}

	interface TestSecondaryServices {}

	interface TestTertiaryServices {}
}