Android Officium
===============

[![CircleCI](https://circleci.com/bb/android-universum/officium.svg?style=shield)](https://circleci.com/bb/android-universum/officium)
[![Codecov](https://codecov.io/bb/android-universum/officium/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/officium)
[![Codacy](https://api.codacy.com/project/badge/Grade/372d0f18b6ba4d08aea97711b500c137)](https://www.codacy.com/app/universum-studios/officium?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/officium&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![OkHttp](https://img.shields.io/badge/okhttp-4.5.0-blue.svg)](https://square.github.io/okhttp)
[![Retrofit](https://img.shields.io/badge/retrofit-2.8.1-blue.svg)](https://square.github.io/retrofit)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Account, synchronization, services and events management for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/officium/wiki)**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aofficium/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aofficium/_latestVersion)

Download the latest **[release](https://bitbucket.org/android-universum/officium/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:officium:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/officium/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 21](http://developer.android.com/about/versions/android-5.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`com.google.code.gson:gson`](https://github.com/google/gson)
- [`com.squareup.okio:okio`](https://github.com/square/okio)
- [`com.squareup.okhttp3:okhttp`](http://square.github.io/okhttp)
- [`com.squareup.retrofit2:retrofit`](http://square.github.io/retrofit)
- [`com.squareup:otto`](http://square.github.io/otto)
- [`universum.studios.android:logger`](https://bitbucket.org/android-universum/logger)
- [`universum.studios.android:crypto`](https://bitbucket.org/android-universum/crypto)

## [License](https://bitbucket.org/android-universum/officium/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.